# 支持WEB、Android、IOS的地图解决方案
### 方案架构
##### 1. 在线解决方案  
![在线解决方案](./ditu/d1.png)  
优点：可以实时更新底图、POI，利用服务端进行路径查找  
缺点：需要网络支持，服务端有流量压力
##### 2. 离线解决方案  
![离线解决方案](./ditu/d2.png)  
优点：不需网络支持，图片显示迅速  
缺点：更新地图时需要下载整个离线地图包
##### 3. 混合解决方案  
![混合解决方案](./ditu/d3.png)  
优点：图片显示迅速，减轻服务端压力。可以实时更新POI，利用服务端进行路径查找  
缺点：实现复杂度高

### 工具链
##### GIS工具集
1. [OpenGeo Suite](http://boundlessgeo.com/solutions/opengeo-suite/download/)  
包含PostGIS, GeoServer, GeoWebCache, OpenLayers, 和 QGIS

##### 地图准备  
1. [QGIS](http://www.qgis.org/) ![QGIS](http://www.qgis.org/en/_static/logo.png)  
导入、导出、编辑.shp,postgis,geotiff,geojson,dxf等多种GIS文件,给普通图片配准到地理坐标  
教程：[http://www.qgistutorials.com/en/](http://www.qgistutorials.com/en/)  

2. [DraftSight](http://www.3ds.com/products-services/draftsight/download-draftsight/)    
编辑CAD文件

3. [SketchUp](http://www.sketchup.com/)  
从CAD文件生成3D视图

##### 地图服务
1. [GeoServer](http://geoserver.org/display/GEOS/Stableop)  
地图服务发布。作为war放入Tomcat或Jetty即可使用。可发布geotiff,.shp,POSTGIS等多种GIS数据为地图。通过openlayer或QGIS浏览。

2. [GeoWebCache](http://geowebcache.org)  
缓存地图瓦片，提高性能。已内置在最新版GeoServer中。

3. [TileStream](https://github.com/mapbox/tilestream‎)  
把.mbtile文件发布为地图服务

##### 离线地图打包
1. [TileMill](https://www.mapbox.com/tilemill/‎)  
将geotiff,geojson,csv,shp,postgis等文件美化、打包为.mbtile瓦片地图文件,存有地图信息的sqlite文件。  
教程:[https://www.mapbox.com/tilemill/docs/crashcourse/introduction/](https://www.mapbox.com/tilemill/docs/crashcourse/introduction/)

2. [mbutil](https://github.com/mapbox/mbutil)  
将.mbtile文件分解为普通图片和json文件

##### 地图浏览
1. [Mapbox.js](https://www.mapbox.com/mapbox.js/) ![Logo](http://2012.desarrollandoamerica.org/files/2012/10/mapbox-logo-horizontal-160x50.png)    
在浏览器中显示TMS地图（Javascript）。基于Leaflet。可配合TileSteam使用。
Example:[https://www.mapbox.com/mapbox.js/example/v1.0.0/](https://www.mapbox.com/mapbox.js/example/v1.0.0/)

2. [Mapbox IOS SDK](https://www.mapbox.com/mapbox-ios-sdk/)  
在IOS设备中显示TMS或.mbtile地图，基于[Route-Me](https://github.com/route-me/route-me)

3. [Mapbox Android SDK](https://github.com/mapbox/mapbox-android-sdk)  
在Android设备中显示TMS或.mbtile地图，基于[osmdroid](https://code.google.com/p/osmdroid/)  
例程：//depot/research/mapstudy/tstmapbox/

4. [OsmDroid](https://code.google.com/p/osmdroid/)    
在Android设备中显示TMS或.mbtile地图  
例程：//depot/research/mapstudy/tstosmdroid/

5. [osmbonuspack](https://code.google.com/p/osmbonuspack/)  
为OsmDroid增加显示POI等功能  
例程：//depot/research/mapstudy/tstosmbonuspack/

6. [Leaflet](http://leafletjs.com/)  
在浏览器中显示TMS地图（Javascript）。

7. [OpenLayers](http://openlayers.org/)  
在浏览器中显示TMS、WMS地图（Javascript）。比Leaflet功能强大复杂。

8. [Indoor.js](https://indoor.io/)  
Indoor 工具集，基于OpenSteetMap,TileMill,Leaflet。 目前尚不稳定。

9. [d3.js](http://d3js.org/)  
Javascript数据显示工具


##### 工具链关系图
![工具链关系图](ditu/mapflow.png =800x)

### 离线地图制作举例  
1. 假设输入为DWG,若原图为位图（JPG/PNG），直接跳到第5步 ![example](ditu/dwg.png)
2. 在SketchUp中导入DWG。并用[Make Faces](http://www.smustard.com/script/MakeFaces) 插件([YouTube](http://www.youtube.com/watch?v=ls_HuZC6bRA))生成面 ![example](ditu/sketchup1.png)
3. 在SketchUp中拉伸为3D模型![example](ditu/sketchup2.png)
4. 导出,如果用[render](http://sketchuprendering.com/rendering-add-ons-for-sketchup/)渲染更好 ![example](ditu/sketchup3.png) 
5. 打开QGIS，"栅格"->配准工具。左边第一个图标打开底图。点击黄色齿轮。设置输出为base.tif。 目标空间参照系统为EPSG:3857. 勾选“完成时载入到QGIS” ![example](ditu/qgisx1.png) 
6. 在图上任点2点。在弹出框内输入该点坐标值。输入像素坐标即可。![example](ditu/qgisx2.png) 
7. 点第二个图标完成配准。![example](ditu/qgis1.png) 
6. 添加一个矢量图层描述信标,类型为“点”，新建属性ccode，类型为整数（信标内容码）。新建属性name,类型为字符串。![example](ditu/qgis2.png) 
7. 点击黄色小铅笔到编辑模式，再点击 * 图标，在图上点击要增加信标的位置。添加3个点，id和ccode分别为1,2,3。再次点击黄色小铅笔退出编辑模式。![example](ditu/qgis3.png)  
8. 添加一个矢量图层描述信标,类型为“多边形”，新建属性name，类型为字符串.
9. 点击黄色小铅笔到编辑模式，再点击 * 图标，在图上勾出围栏区域。并分别命名。![example](ditu/qgis4.png) 
10. 右键位图图层，另存为GeoTiff ![example](ditu/qgis5.png) 
11. 右键矢量图层。另存为GeoJson ![example](ditu/qgis6.png) 
12. 打开TileMill,Project-->New project
13. Layers-->Add Layer, 选择刚才导出的base.tif,SRS选择900913. “Save & Style” 。点击图层#base上得放大镜缩放到合适大小。![example](ditu/tilemill1.png) 
14. 删除Contries图层和相应的mss。把background-color改为和底图一致 ![example](ditu/tilemill2.png) 
14. "+ Add Layer" 添加刚才导出的2个geojson文件![example](ditu/tilemill3.png) 
15. 选择左侧手指==>Teaser.下面图层选择为hotarea.上面内容框中填入{{{name}}} ![example](ditu/tilemill4.png) 
16. Save以后。可以看到热区效果。 当鼠标移到热区时。相应的name会显示。![example](ditu/tilemill5.png) 
17. 将polygon-opacity,line-opacity和marker-opacity设为0。点击Export导出mbtiles. 放大图片到zoom 16级，把中心标志放到图中央。shift+鼠标拖动框选地图区域。点击 Export导出为文件。在~/Documents/MapBox/export中可以找到导出的example.mbtiles文件 ![example](ditu/tilemill6.png) 
18. 启动地图服务。在terminal中执行  tilestream --tiles=/Users/fangjian/Documents/MapBox/export 然后访问http://localhost:8888/ 可以看到地图已经生效。访问 http://localhost:8888/v2/example.json 可以下载tilejson. 访问 http://localhost:8888/v2/example.mbtiles可以下载mbtiles文件。 如果只制作离线地图可以跳过这步。![example](ditu/tilestream1.png) 
19. 解包example.mbtiles为普通图片和json文件.在.mbtiles所在目录。运行mb-util example.mbtile example   
图片文件被解到了example目录下
20. 复制metadata.json为tile.json 改为example.json的格式 。将center和bounds改为数组（如果只做离线文件可以跳过）
21. 参考metadata.json 修改 https://www.mapbox.com/mapbox.js/example/v1.0.0/ 样例

		<script>
		var map = L.mapbox.map('map');
		var stamenLayer = L.tileLayer('./{z}/{x}/{y}.png', {
		  attribution: 'Map tiles by <a href="http://dreamvoc.com">DreamVoc</a>.',
		  minZoom:14,
		  maxZoom:17
		}).addTo(map);
		map.setView([-0.0047,0.0075], 16);
		</script>
22. 拷贝ccode.geojson和hotarea.geojson到当前目录。编辑文件内容为var ccode={... var hotarea={...
	map.featureLayer.setGeoJSON(ccode); 然后加入以下代码
	
		<script src="ccode.geojson" ></script>
		<script src="hotarea.geojson" ></script>
		<script>
		map.featureLayer.setGeoJSON(ccode);
		</script>
		<script>
		L.geoJson(hotarea).addTo(map);
		</script>
23. 参考[http://leafletjs.com/examples/choropleth.html](http://leafletjs.com/examples/choropleth.html)加入热区 ![example](ditu/xg1.png)  ![example](ditu/xg2.png) 
	