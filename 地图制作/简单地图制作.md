# 简单地图制作
1. 找一张图，比如 http://img.pchomeus.com/img/product/C/5/Y/9/P01D000C5Y9/D4f694aa69b3f9_4f6c23701f8bd.jpg
![动物园](./img/qgis2.png) 
`原始图片没有坐标信息，需要转换为有坐标的GeoTiff格式`
2. 打开QGIS，"栅格"->配准工具。左边第一个图标打开底图。
![image](./img/peizhun.png) 
`配准工具的目的是给赋予图片和坐标的对应关系，至少需要2个点。`
3. 在图上任点2点。在弹出框内输入该点坐标值。输入像素坐标即可。  点击黄色齿轮。设置输出为zoo.tif。 目标空间参照系统为EPSG:3857. 勾选“完成时载入到QGIS”  。点击绿色箭头完成配准。
![image](./img/peizhun3.png)  
![image](./img/peizhun4.png) 
![image](./img/peizhun5.png) 
`EPSG:3857是墨卡托投影,是数字地图常用的坐标体系,等价于900913,但不是经纬度。经纬度坐标是EPSG:4326`
4. 用TileMill新建工程  
![image](./img/tm1.png)   
5. 点击左下角图层图标，增加图层(Add Layer), Datasource选择刚才的tif文件，SRS选择900913. “Save & Style” 。点击图层上的放大镜缩放到合适大小。
![image](./img/layer1.png)   
6. 删除Contries图层和相应的mss。把background-color改为需要的颜色 
![image](./img/layer2.png)   
7. Export 为mbtiles文件。点击放大镜放大到所需级别（如18）。 用shift+鼠标框选地图，设置地图范围。勾选“Save settings to project”。拖动ZOOM范围缩小到可视范围附近（如17~20）。点击Export 导出zoo.mbtiles文件。
![image](./img/export0.png)  
![image](./img/export1.png)  
![image](./img/export2.png)  
`地图上看不到自己的图，可能是放大级别不够或者离中心太远。地图范围和ZOOM级别决定了生成的瓦片的数量，也就决定了地图的大小`
8. 将zoo.mbtiles文件拷贝到例程的assets文件夹，(以安卓版为例) 修改MainActivity中几个地方。

		private XYTileSource MBTILESRENDER = new XYTileSource(
			"mbtiles", 
			ResourceProxy.string.offline_mode, 
			17, 20,  // zoom min/max <- should be taken from metadata if available 
			256, ".png", urls);
	
	放大级别17~20
	
		String mapfilename="zoo.mbtiles";
	文件名，没啥好说的
	
		GeoPoint point2 = new GeoPoint(-0.0026,0.0050); //经纬度 在QGIS中对应EPSG:3826坐标
	坐标点。注意这里是经纬度。在QGIS里切换经纬度设置可以看到。点击右下角的坐标球可以切换坐标系。
	![image](./img/qgis_zb.png)  