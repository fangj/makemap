package com.example.tstosmdroid;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import org.osmdroid.api.IMapController;
import org.osmdroid.bonuspack.kml.KmlDocument;
import org.osmdroid.bonuspack.location.NominatimPOIProvider;
import org.osmdroid.bonuspack.location.POI;
import org.osmdroid.bonuspack.overlays.FolderOverlay;
import org.osmdroid.bonuspack.overlays.GroundOverlay;
import org.osmdroid.bonuspack.overlays.InfoWindow;
import org.osmdroid.bonuspack.overlays.Marker;
import org.osmdroid.bonuspack.overlays.Marker.OnMarkerClickListener;
import org.osmdroid.bonuspack.overlays.Marker.OnMarkerDragListener;
import org.osmdroid.bonuspack.overlays.MarkerInfoWindow;
import org.osmdroid.bonuspack.overlays.Polyline;
import org.osmdroid.bonuspack.routing.OSRMRoadManager;
import org.osmdroid.bonuspack.routing.Road;
import org.osmdroid.bonuspack.routing.RoadManager;
import org.osmdroid.bonuspack.routing.RoadNode;
import org.osmdroid.tileprovider.MapTileProviderArray;
import org.osmdroid.tileprovider.modules.IArchiveFile;
import org.osmdroid.tileprovider.modules.MBTilesFileArchive;
import org.osmdroid.tileprovider.modules.MapTileFileArchiveProvider;
import org.osmdroid.tileprovider.modules.MapTileModuleProviderBase;
import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.tileprovider.tilesource.XYTileSource;
import org.osmdroid.tileprovider.util.SimpleRegisterReceiver;
import org.osmdroid.util.BoundingBoxE6;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import org.osmdroid.DefaultResourceProxyImpl;
import org.osmdroid.ResourceProxy;

/**
 * This is the implementation of OSMBonusPack tutorials. 
 * Sections of code can be commented/uncommented depending on the progress in the tutorials. 
 * @see http://code.google.com/p/osmbonuspack/
 * @author M.Kergall
 *
 */
public class MainActivity extends Activity implements OnMarkerClickListener{

	Context mContext;
	MapView map;
	Button button1;
	Button button2;
	String[] urls={"http://i.dont.care.org/"};
	// Most of this is useless
	private XYTileSource MBTILESRENDER = new XYTileSource(
			"mbtiles", 
			ResourceProxy.string.offline_mode, 
			17, 20,  // zoom min/max <- should be taken from metadata if available 			 
			256, ".png", urls);
	
	@Override protected void onCreate(Bundle savedInstanceState) {
		mContext=this;
		
		//Introduction
		super.onCreate(savedInstanceState);
		DefaultResourceProxyImpl mResourceProxy = new DefaultResourceProxyImpl(this.getApplicationContext());
		SimpleRegisterReceiver simpleReceiver = new SimpleRegisterReceiver(this);
		String mapfilename="zoo.mbtiles";
		File f = new File("/data/data/" + this.getPackageName() + "/" + mapfilename);
		if(f.exists()){
		 f.delete();
		}
		//if(!f.exists()){
			IoUtils.copyFile(this,mapfilename);
		//}
			
		IArchiveFile[] files = { MBTilesFileArchive.getDatabaseFileArchive(f) };
		 
		MapTileModuleProviderBase moduleProvider = new MapTileFileArchiveProvider(simpleReceiver, MBTILESRENDER, files);
		 
		MapTileProviderArray mProvider = new MapTileProviderArray(MBTILESRENDER, null, new MapTileModuleProviderBase[] { moduleProvider });
		 
		this.map = new MapView(this, 256, mResourceProxy, mProvider);
		this.setContentView(R.layout.main);    	
		FrameLayout rl = (FrameLayout) findViewById(R.id.maplayout);
	
		rl.addView(this.map);
		map.setBuiltInZoomControls(true);
		map.setMultiTouchControls(true);
		BoundingBoxE6 bbox = new BoundingBoxE6(0.5, 0.5, -0.5, -0.5); //边界
		map.setScrollableAreaLimit(bbox);
		
		GeoPoint startPoint = new GeoPoint(0, 0);
		IMapController mapController = map.getController();
		mapController.setZoom(18); //初始大小
		mapController.setCenter(startPoint);

		//0. Using the Marker overlay
		final Marker marker1 = new Marker(map);
		marker1.setPosition(startPoint);
		marker1.setAnchor(Marker.ANCHOR_CENTER, 1.0f);
		marker1.setTitle("机器人");
		marker1.setSnippet("1");
		map.getOverlays().add(marker1);
		marker1.setOnMarkerClickListener(this);
		InfoWindow infoWindow = new MarkerInfoWindow(R.layout.bonuspack_bubble, map);
		marker1.setInfoWindow(infoWindow);
		Button button1=(Button)infoWindow.getView().findViewById(R.id.button1);
		button1.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View arg0) {
				Toast.makeText(mContext, "button1 clicked", Toast.LENGTH_SHORT).show();
			}
		});
		
		//maker2
		final Marker marker2 = new Marker(map);
		GeoPoint point2 = new GeoPoint(-0.0026,0.0050); //经纬度 在QGIS中对应EPSG:3826坐标
		marker2.setPosition(point2);
		marker2.setAnchor(Marker.ANCHOR_CENTER, 1.0f);
		marker2.setTitle("袋鼠");
		marker2.setSnippet("2");
		map.getOverlays().add(marker2);
		marker2.setOnMarkerClickListener(this);
		InfoWindow infoWindow2 = new MarkerInfoWindow(R.layout.bonuspack_bubble, map);
		marker2.setInfoWindow(infoWindow2);
		Button button2=(Button)infoWindow2.getView().findViewById(R.id.button1);
		button2.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View arg0) {
				Toast.makeText(mContext, "button2 clicked", Toast.LENGTH_SHORT).show();		
			}
		});
		
		//set button
		button1=(Button)findViewById(R.id.button1);
		button1.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View arg0) {
				map.getController().setCenter(marker1.getPosition());			
			}});
		
		//set button
		button2=(Button)findViewById(R.id.button2);
		button2.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View arg0) {
				map.getController().setCenter(marker2.getPosition());
			}});
	}
	@Override
	public boolean onMarkerClick(Marker marker, MapView arg1) {
		marker.showInfoWindow();
		Log.d("test",marker.getSnippet());
		Toast.makeText(this, "click marker"+marker.getSnippet(), Toast.LENGTH_SHORT).show();
		return false;
	}

	
}
